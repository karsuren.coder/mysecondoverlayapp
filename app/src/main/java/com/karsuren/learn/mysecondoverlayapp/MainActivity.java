package com.karsuren.learn.mysecondoverlayapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.karsuren.learn.mysecondoverlayapp.util.L;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        L.l("entered MainActivity.onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button startOverlayButton = (Button) findViewById(R.id.start_overlay_bt);
        startOverlayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startOverlay();
            }
        });
    }

    @Override
    protected void onStart() {
        L.l("entered MainActivity.onStart");
        super.onStart();
    }

    @Override
    protected void onResume() {
        L.l("entered MainActivity.onResume");
        super.onResume();
    }

    @Override
    protected void onPause() {
        L.l("entered MainActivity.onPause");
        super.onPause();
    }

    @Override
    protected void onStop() {
        L.l("entered MainActivity.onStop");
        super.onStop();
    }

    @Override
    protected void onRestart() {
        L.l("entered MainActivity.onRestart");
        super.onRestart();
    }

    private void startOverlay() {
        startService(new Intent(getApplicationContext(), OverlayService.class));
    }
}