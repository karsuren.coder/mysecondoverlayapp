package com.karsuren.learn.mysecondoverlayapp.util;

import android.util.Log;

public class L {
    public static final String TAG = "surenkar";

    public static void l(String message){
        Log.d(TAG, message);
    }
}
