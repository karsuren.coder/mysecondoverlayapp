package com.karsuren.learn.mysecondoverlayapp;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.Build;
import android.os.IBinder;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.karsuren.learn.mysecondoverlayapp.util.L;

public class OverlayService extends Service {

    WindowManager mWindowManager;
    LayoutInflater mLayoutInflator;
    WindowManager.LayoutParams mLayoutParams;
    View mOverlayRootView;

    boolean mVisible;

    public OverlayService() {
        L.l("entered OverlayService:constructor");
        mVisible = false;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        L.l("entered OverlayService.onCreate");
        super.onCreate();

        mWindowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        mLayoutInflator = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mLayoutParams = getOverlayLayoutPrams();

        mOverlayRootView = mLayoutInflator.inflate(R.layout.overlay_layout, null);

        initUIBehaviour();

        showOverlay();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        L.l("entered OverlayService.onStartCommand");
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        L.l("entered OverlayService.onDestroy");
        super.onDestroy();

        hideOverlay();
    }

    private void showOverlay() {
        if (!mVisible) {
            mWindowManager.addView(mOverlayRootView, mLayoutParams);
            mVisible = true;
        }
    }

    private void hideOverlay() {
        if (mVisible) {
            mWindowManager.removeView(mOverlayRootView);
            mVisible = false;
        }
    }

    private WindowManager.LayoutParams getOverlayLayoutPrams() {
        DisplayMetrics metrics = getResources().getDisplayMetrics();

        int width = metrics.widthPixels/2;
        int height = metrics.heightPixels/2;

        int layoutType = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) ?
                WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY :
                WindowManager.LayoutParams.TYPE_PHONE;

        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams(width,
                height,
                layoutType,
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
                PixelFormat.TRANSLUCENT);

        layoutParams.gravity = Gravity.CENTER;
        layoutParams.x = 0;
        layoutParams.y = 0;

        return layoutParams;
    }

    private void initUIBehaviour() {
        final Button closeOverlayButton = mOverlayRootView.findViewById(R.id.close_overlay_button);
        closeOverlayButton.setOnClickListener(view -> {
            OverlayService.this.stopSelf();
        });

        final EditText toastTextField = mOverlayRootView.findViewById(R.id.toast_edit_text);

        final Button toastButton = mOverlayRootView.findViewById(R.id.toast_button);
        toastButton.setOnClickListener(view -> {
            String toastMessage = toastTextField.getText().toString();
            Toast.makeText(OverlayService.this.getApplicationContext(),
                    toastMessage,
                    Toast.LENGTH_SHORT).show();
        });
    }
}